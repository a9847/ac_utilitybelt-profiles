﻿# This script is for copying files to the Utiliy Belt profile directory

$UtilityBeltDirectory = "$ENV:UserProfile\Documents\Decal Plugins\UtilityBelt" # "$ENV:UserProfile\Documents\Decal Plugins\UtilityBelt"



# Update Files
Copy-Item "$PSScriptRoot\*\" "$UtilityBeltDirectory" -Exclude .git -Recurse -Force -Verbose

pause # pause so output can be read