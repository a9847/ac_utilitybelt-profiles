# Utility Belt Profiles

These are utility belt profiles to help automate tasks such as recomp or muling loot.
- [UilityBelt autovendor documentation](https://utilitybelt.gitlab.io/docs/tools/autovendor/)

This folder shold be placed in your UtilityBelt settings folder, %userprofile%\Documents\Decal Plugins\UtilityBelt
E.G. C:\Users\YOUUSERNAME\Documents\Decal Plugins\UtilityBelt

These files should either be downloaded from GitLab as a .zip file and then extracted or checked out/cloned using the Git client.  Attempting to download an individual file will result in GitLab changing the formatting of the file, preventing it from working.

For details see the linked readmes below.

[Autovendor](Drunkenfell/autovendor/Readme.MD) <br>
 %userprofile%\Documents\Decal Plugins\UtilityBelt\Drunkenfell\autovendor 


[ItemGiver](itemgiver/Readme.MD) <br>
 %userprofile%\Documents\Decal Plugins\UtilityBelt\itemgiver

 Copy-Files.ps1 <br>
 Script for copying these files to their respeective UtilityBelt Directories
 

 ## Disclaimer

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.